# HONEYCOMB

[![Documentation](https://godoc.org/gitlab.com/passelecasque/honeycomb?status.svg)](https://godoc.org/gitlab.com/passelecasque/honeycomb)
 [![Go Report Card](https://goreportcard.com/badge/gitlab.com/passelecasque/honeycomb)](https://goreportcard.com/report/gitlab.com/passelecasque/honeycomb) [![codecov](https://codecov.io/gl/passelecasque/honeycomb/branch/master/graph/badge.svg)](https://codecov.io/gl/passelecasque/honeycomb) [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](https://opensource.org/licenses/MIT) 
[![Build status](https://gitlab.com/passelecasque/honeycomb/badges/master/build.svg)](https://gitlab.com/passelecasque/honeycomb/pipelines)
