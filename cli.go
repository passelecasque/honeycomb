package main

import (
	"fmt"

	"github.com/docopt/docopt-go"
	"github.com/pkg/errors"
	"gitlab.com/catastrophic/assistance/fs"
)

const (
	usage = `
	_  _ ____ _  _ ____ _   _ ____ ____ _  _ ___  
	|__| |  | |\ | |___  \_/  |    |  | |\/| |__] 
	|  | |__| | \| |___   |   |___ |__| |  | |__] 
	
Description:
    
	Honeycomb sorts through a download directory and extracts
	folders belonging to specific torrents.
	
Usage:
    honeycomb [-n] [-m] <TORRENT_DIR> <DOWNLOAD_DIR> <DESTINATION_DIR> 

Options:
    -n                     Dry run
    -m                     Move files instead of copying them
    -h, --help             Show this screen.
    --version              Show version.
`
	fullName    = "honeycomb"
	fullVersion = "%s -- v%s"
	version     = "0.1.0"
)

type honeycombArgs struct {
	builtin        bool
	dryRun         bool
	moveFile       bool
	torrentDir     string
	downloadDir    string
	destinationDir string
}

func (b *honeycombArgs) parseCLI(osArgs []string) error {
	// parse arguments and options
	args, err := docopt.Parse(usage, osArgs, true, fmt.Sprintf(fullVersion, fullName, version), false, false)
	if err != nil {
		return errors.Wrap(err, "incorrect arguments")
	}
	if len(args) == 0 {
		// builtin command, nothing to do.
		b.builtin = true
		return nil
	}
	b.dryRun = args["-n"].(bool)
	b.moveFile = args["-m"].(bool)
	b.torrentDir = args["<TORRENT_DIR>"].(string)
	if !fs.DirExists(b.torrentDir) {
		return errors.New("invalid torrent directory")
	}
	b.downloadDir = args["<DOWNLOAD_DIR>"].(string)
	if !fs.DirExists(b.downloadDir) {
		return errors.New("invalid download directory")
	}
	b.destinationDir = args["<DESTINATION_DIR>"].(string)
	if !fs.DirExists(b.downloadDir) {
		return errors.New("invalid destination directory, create it if necessary")
	}
	if b.downloadDir == b.destinationDir {
		return errors.New("destination directory cannot be the downloads directory")
	}

	return nil
}
