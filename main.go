package main

import (
	"fmt"
	"os"
	"path/filepath"
	"strconv"

	"github.com/anacrolix/torrent/metainfo"
	"gitlab.com/catastrophic/assistance/fs"
	"gitlab.com/catastrophic/assistance/logthis"
)

const (
	torrentExt = ".torrent"
)

func main() {
	// parsing CLI
	cli := &honeycombArgs{}
	if err := cli.parseCLI(os.Args[1:]); err != nil {
		fmt.Println(err.Error())
		return
	}
	if cli.builtin {
		return
	}

	if cli.dryRun {
		logthis.Info("Dry run mode activated, nothing will be moved.", logthis.NORMAL)
	}

	// list all torrent files in cli.torrentDir
	torrents, err := fs.GetFilesByExt(cli.torrentDir, torrentExt)
	if err != nil {
		logthis.Error(err, logthis.NORMAL)
		return
	}
	if len(torrents) == 0 {
		logthis.Info("No torrents found in "+cli.torrentDir, logthis.NORMAL)
		return
	}

	var movedTorrents int
	for _, t := range torrents {
		var moved bool
		// get folder or main file in torrent
		m, err := metainfo.LoadFromFile(t)
		if err != nil {
			logthis.Error(err, logthis.NORMAL)
			continue
		}
		info, err := m.UnmarshalInfo()
		if err != nil {
			logthis.Error(err, logthis.NORMAL)
			continue
		}
		// checking
		if info.Name == "" {
			logthis.Info("Cannot find content for torrent "+t+", ignoring.", logthis.NORMAL)
			continue
		}
		// moving if relevant
		fmt.Println(" >> " + info.Name)
		origin := filepath.Join(cli.downloadDir, info.Name)
		destination := filepath.Join(cli.destinationDir, info.Name)
		if fs.FileExists(origin) {
			if !fs.FileExists(destination) {
				if !cli.dryRun {
					if err := fs.CopyFile(origin, destination, false); err != nil {
						logthis.Error(err, logthis.NORMAL)
						continue
					}
					if cli.moveFile {
						if err := os.RemoveAll(origin); err != nil {
							logthis.Error(err, logthis.NORMAL)
							continue
						}
					}
				}
				moved = true
			} else {
				logthis.Info("File(s) already in destination for torrent "+t+", ignoring.", logthis.NORMAL)
			}
		} else if fs.DirExists(origin) {
			if !fs.DirExists(destination) {
				if !cli.dryRun {
					if err := fs.CopyDir(origin, destination, false); err != nil {
						logthis.Error(err, logthis.NORMAL)
						continue
					}
					if cli.moveFile {
						if err := os.RemoveAll(origin); err != nil {
							logthis.Error(err, logthis.NORMAL)
							continue
						}
					}
				}
				moved = true
			} else {
				logthis.Info("File(s) already in destination for torrent "+t+", ignoring.", logthis.NORMAL)
			}
		} else {
			logthis.Info("File(s) not found for torrent "+t+".", logthis.NORMAL)
		}
		if moved {
			movedTorrents++
		}
	}
	if cli.moveFile {
		fmt.Println("Moved files for " + strconv.Itoa(movedTorrents) + "/" + strconv.Itoa(len(torrents)) + " torrents.")
	} else {
		fmt.Println("Copied files for " + strconv.Itoa(movedTorrents) + "/" + strconv.Itoa(len(torrents)) + " torrents.")
	}
}
